from qgis.core import QgsProject, QgsLayoutPoint, QgsLayoutSize

project = QgsProject.instance()
manager = project.layoutManager()

def openProject():
    manager.layoutByName("Forstwirtschaftskarte (FWK)").itemById("zeichenerklaerung").sizePositionChanged.connect(updateOverviewLocation_FWK)
    manager.layoutByName("Forstgrundkarte (FGK)").itemById("zeichenerklaerung").sizePositionChanged.connect(updateOverviewLocation_FGK)
    manager.layoutByName("Forstübersichtskarte (FUEK)").itemById("zeichenerklaerung").sizePositionChanged.connect(updateOverviewLocation_FUEK)

def saveProject():
    pass

def closeProject():
    pass

def updateOverviewLocation(layout,legend,overview):
    p = legend.pagePositionWithUnits() # get legend position
    s = legend.sizeWithUnits()         # get legend size
    s_o = overview.sizeWithUnits()     # get overview size
    
    # new position for overview
    overview_new_pos = QgsLayoutPoint(p.x(), p.y() + s.height() + 1)
    # new size for overview
    overview_new_size = QgsLayoutSize(s.width(), s_o.height())
    
    # change position and size of overview
    overview.attemptMove(overview_new_pos)
    overview.attemptResize(overview_new_size)
    
    layout.refresh()
    layout.updateBounds()

def updateOverviewLocation_FWK():
    layout = manager.layoutByName("Forstwirtschaftskarte (FWK)")
    legend = layout.itemById("zeichenerklaerung")
    overview = layout.itemById("overview_and_info")
    updateOverviewLocation(layout,legend,overview)

def updateOverviewLocation_FGK():
    layout = manager.layoutByName("Forstgrundkarte (FGK)")
    legend = layout.itemById("zeichenerklaerung")
    overview = layout.itemById("overview_and_info")
    updateOverviewLocation(layout,legend,overview)

def updateOverviewLocation_FUEK():
    layout = manager.layoutByName("Forstübersichtskarte (FUEK)")
    legend = layout.itemById("zeichenerklaerung")
    overview = layout.itemById("overview_and_info")
    updateOverviewLocation(layout,legend,overview)