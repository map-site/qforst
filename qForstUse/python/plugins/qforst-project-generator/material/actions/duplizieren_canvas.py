from qgis.utils import iface
from qgis.core import QgsProject, QgsVectorFileWriter, QgsCoordinateReferenceSystem

project = QgsProject.instance()
layer   = QgsProject.instance().mapLayer('[% @layer_id %]')

# Falls der Editiermodus nicht aktiviert ist, Benutzer benachrichtigen
if not layer.isEditable():
    iface.messageBar().pushWarning("qForst", "Sie sind nicht im Editier-Modus!")

# Andernfalls kanns los gehen...
else:
    feature = layer.getFeature( [% $id %] )

    # Feature das ausgewählt wird duplizieren
    new_feat = QgsVectorLayerUtils.duplicateFeature(layer, feature, project, 0)[0]

    # Den FeatureForm-Dialog anzeigen
    success = iface.openFeatureForm(layer, new_feat, True)

    # Wenn erfolgreich, zeige Benachrichtigung und speichere die Änderungen in dem Layer
    if success: 
        iface.messageBar().pushSuccess("qForst", "Neuer Bestand wurde als Flächentyp \"" + new_feat["typ"] + "\" angelegt. Sie können nun Schneiden...")
        layer.removeSelection()
        layer.select(new_feat.id())
    else:
        layer.deleteFeature(new_feat.id())
        iface.messageBar().pushWarning("qForst", "Verdoppeln abgebrochen!")