from qgis.utils import iface
from qgis.core import QgsProject, QgsVectorFileWriter, QgsCoordinateReferenceSystem

project = QgsProject.instance()
layer   = QgsProject.instance().mapLayer("[% @layer_id %]")

# Prüfen, ob genau ein Feature ausgewählt wurde
if layer.selectedFeatureCount()==1:
    
    # Checken, ob Editiermodus aktiv ist:
    if not layer.isEditable():
        iface.messageBar().pushWarning("qForst", "Sie sind nicht im Editier-Modus!")

    else:
        # Das ausgewählte Feature duplizieren
        new_feat = QgsVectorLayerUtils.duplicateFeature(layer, layer.selectedFeatures()[0], project, 0)[0]

        # Den FeatureForm-Dialog anzeigen
        success = iface.openFeatureForm(layer, new_feat, True)

        # Wenn erfolgreich, zeige Benachrichtigung und speichere die Änderungen in dem Layer
        if success: 
            iface.messageBar().pushSuccess("qForst", "Neuer Bestand wurde als Flächentyp \"" + new_feat["typ"] + "\" angelegt. Sie können nun Schneiden...")
            layer.removeSelection()
            layer.select(new_feat.id())
        else:
            layer.deleteFeature(new_feat.id())
            iface.messageBar().pushWarning("qForst", "Verdoppeln abgebrochen!")

# Falls 0 oder mehr als 1 Feature ausgewählt wurden, zeige entsprechende Fehlermeldung und tue sonst nichts
else:
    iface.messageBar().pushWarning("qForst", "Bitte die zu verdoppelnde Bestandsfläche auswählen!")
