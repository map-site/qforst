# Checkliste für qForst-Installation

#### Profil qForstUse installieren (ohne QGIS.ini - erzeugt QGIS systemabhängig selbstständig)

Windows: 

    C:\Users\admin\AppData\Roaming\QGIS\QGIS3\profiles

Linux:

    /home/[user]/.local/share/QGIS/QGIS3/profiles

Da die QGIS.ini, welche die Grundeinstellungen von QGIS bestimmt, nicht ausgetauscht werden darf, muss man auf folgende Grundeinstellungen achten:

 - Oberflächenanapssung in QGIS aktivieren: Einstellungen -> Oberflächenanapssung
 - Layerauswahlreihenfolge auf Layerauswahl setzen
 - Formular automatisch öffnen
 - 3mm Gummiband
 - Macros permanent aktivieren (auch wenn diese noch nicht implementiert sind)
 - KBS festlegen

#### Lizenzhalter in qforst.qgz eintragen ####

 - in der ...profiles/qForstUse/python/plugins/qforst-project-generator/material/qForst.qgz unter ''qforst-lizenz'' den Lizenznehmer eintragen

#### Splash Screen

`splashpath`  in folgender Datei anpassen (im von QGIS tatsächlich verwendeten profiles-Ordner!): `profiles/qForstUse/QGIS/QGISCUSTOMIZATION3.ini`

Bei Sebi also zB: 

Linux:

    splashpath=/media/sebastian/Ulli/Map-Site/qforst/material/profiles/qForstUse/QGIS/

Windows: 

    splashpath=C:\\Users\\Sebastian\\AppData\\Roaming\\QGIS\\QGIS3\\profiles\\qForstUse\\QGIS\\


**Wichtig:** Auf genaue Schreibweise achten. Einfacher "/" in Linux, doppelter "\\\\" in Windows! Und "/" bzw "\\\\" am Ende nicht vergessen!!


#### Schriftart Open Sans installiert (falls nicht vorhanden)

Donwload & Install: https://fonts.google.com/specimen/Open+Sans
